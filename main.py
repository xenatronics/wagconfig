import sys
import os
import subprocess
import time

from PySide6.QtCore import Qt, QThreadPool, QUrl
from PySide6.QtWidgets import QApplication, QMainWindow, QFileDialog, QTreeWidgetItem, QProgressDialog
from base import Ui_WagConfig  # module ui
from check import show_python_version, show_python_path, show_wagtail_version
from db_sqlite3 import DatabaseManager
from PySide6.QtGui import QStandardItemModel, QStandardItem
from utils import ApplicationCreator, CommandCreator, Couleur, Command


def message_color(message: str, type_message: Couleur):
    font_proto = "<font color='white'>"
    if type_message == Couleur.SUCCESS:
        font_proto = "<font color='lightgreen'>"
    if type_message == Couleur.ERROR:
        font_proto = "<font color='orangered'>"
    if type_message == Couleur.NEUTRAL:
        pass
    return font_proto + message + "</font>"


# Class dérivée de Ui_WagConfig (QtDesigner) et de QMainWindow
class MyWindow(QMainWindow, Ui_WagConfig):
    def __init__(self):
        super().__init__()
        self.command = None
        self.setupUi(self)
        self.init_connect()
        self.init_check()
        self.app_dir = ""
        self.app_name = ""
        self.db_dir = ""
        self.db_manager = None
        self.model = QStandardItemModel()
        # self.wagtail = None
        self.thread_pool = QThreadPool.globalInstance()
        self.creator = None
        # self.table_db.setModel(self.model)
        # self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
        self.center()
        self.message = ""
        self.progress_dialog = None
        self.url = ""

    def init_check(self):
        self.python_version.setText(show_python_version())
        self.python_path.setText(show_python_path())
        self.wagtail_version.setText(show_wagtail_version())

    def init_connect(self):
        self.pushButton.clicked.connect(self.clear_console)
        self.pushButton_4.clicked.connect(self.get_directory_db)
        self.browse_path_app.clicked.connect(self.get_directory_app)
        self.btn_connect_db.clicked.connect(self.connect_database)
        self.tree_db.itemClicked.connect(self.on_tree_item_clicked)
        self.btn_create_app.clicked.connect(self.create_app)
        self.btn_start_server.clicked.connect(self.start_server)
        self.btn_start_admin.clicked.connect(self.start_admin)

    def center(self):
        # Obtenir la géométrie de l'écran
        screen_geometry = QApplication.primaryScreen().geometry()
        # Calculer les coordonnées x et y pour centrer la fenêtre
        x = (screen_geometry.width() - self.width()) // 2
        y = 2
        # Déplacer la fenêtre au centre
        self.move(x, y)

    def clear_console(self):
        self.lbl_output.setText("")

    def get_directory_app(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)  # Sélection d'un répertoire
        dialog.setViewMode(QFileDialog.List)  # Affichage en mode liste
        if dialog.exec():
            self.app_dir = dialog.selectedFiles()[0]
            self.text_path_app.setText(self.app_dir)
            print(f"Répertoire sélectionné : {self.app_dir}")
        else:
            print("Aucun répertoire sélectionné.")

    def create_app(self):
        self.app_name = self.text_name_app.toPlainText()
        self.app_dir = self.text_path_app.toPlainText()
        if self.app_dir.strip() == "" or self.app_name == "":
            return
        try:
            self.creator = ApplicationCreator(self.app_name, self.app_dir)
            self.creator.progress.connect(self.update_progress)
            self.thread_pool.start(self.creator)
            self.progress_dialog = QProgressDialog("Traitement en cours...", None, 0, 100, self)
            self.progress_dialog.setWindowTitle("Génération de l'application")
            self.progress_dialog.setAutoClose(True)
            self.progress_dialog.exec()
        except Exception as e:
            print(f"Erreur lors de la connexion du signal : {e}")

    def execute_command(self, type_cmd: Command = Command.RUNSERVER):
        self.app_dir = self.text_path_app.toPlainText()
        if self.app_dir.strip() == "":
            return
        try:
            self.command = CommandCreator(self.app_dir, type_cmd)
            self.command.status.connect(self.update_command)
            self.command.browser_start.connect(self.refresh_web)
            self.thread_pool.start(self.command)
        except Exception as e:
            print(f"Impossible de connecter la commande : {e}")

    def start_server(self):
        if self.txt_address_server.toPlainText() == "":
            return
        self.url = self.txt_address_server.toPlainText() + ":" + str(self.spin_port_server.text())
        if not self.command:
            self.execute_command()

    def start_admin(self):
        self.url = self.txt_address_server.toPlainText() + ":" + str(self.spin_port_server.text())+ "/admin"
        if not self.command:
            self.execute_command()

        if self.txt_address_server.toPlainText() == "":
            return
        self.refresh_web()

    def refresh_web(self):

        self.view_server.setUrl(QUrl(self.url))
        self.view_server.show()

    def update_command(self, value, message, code):
        start = "<html><body>"
        end = "</body></html>"

        if code == 0:
            self.message += message_color(message, Couleur.SUCCESS)
        elif code == 1:
            self.message += message_color(message, Couleur.ERROR)
        else:
            self.message += message_color(message, Couleur.NEUTRAL)
        self.message += "<br>"
        msg_output = start + self.message + end
        self.lbl_output.setText(msg_output)
        self.lbl_output.adjustSize()
        self.lbl_output.setFixedWidth(self.scrollArea.viewport().width())
        self.scrollAreaWidgetContents.setMinimumHeight(self.lbl_output.height())
        self.scrollArea.verticalScrollBar().setValue(self.scrollArea.verticalScrollBar().maximum())

    def update_progress(self, value, message, code):

        start = "<html><body>"
        end = "</body></html>"

        if code == 0:
            self.message += message_color(message, Couleur.SUCCESS)
        elif code == 1:
            self.message += message_color(message, Couleur.ERROR)
        else:
            self.message += message_color(message, Couleur.NEUTRAL)
        self.message += "<br>"
        self.progress_dialog.setValue(value)
        msg_output = start + self.message + end
        self.lbl_output.setText(msg_output)
        self.lbl_output.adjustSize()
        self.lbl_output.setFixedWidth(self.scrollArea.viewport().width())
        self.scrollAreaWidgetContents.setMinimumHeight(self.lbl_output.height())
        self.scrollArea.verticalScrollBar().setValue(self.scrollArea.verticalScrollBar().maximum())

    # Base de données
    def get_directory_db(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)  # Sélection d'un répertoire
        dialog.setViewMode(QFileDialog.List)  # Affichage en mode liste
        if dialog.exec():
            self.db_dir = dialog.selectedFiles()[0]
            self.textEdit.setText(self.db_dir)
            self.db_manager = DatabaseManager(self.db_dir + "/db.sqlite3")
            print(f"Répertoire sélectionné : {self.db_dir}")
        else:
            print("Aucun répertoire sélectionné.")

    def connect_database(self):
        if self.db_manager.connect():
            self.prepare_data()

    def on_tree_item_clicked(self, item, column):
        # Lorsqu'un élément du QTreeWidget est cliqué
        if item is not None:  # Ne traitez que les tables, pas les colonnes
            table_name = item.text(0)
            print(table_name)
            self.model.clear()
            fields, records = self.db_manager.get_table_data(table_name)
            self.model.setColumnCount(len(fields))
            self.model.setHorizontalHeaderLabels(fields)
            for record in records:
                row_items = [QStandardItem(str(field)) for field in record]
                self.model.appendRow(row_items)

            self.table_db.setModel(self.model)

    def prepare_data(self):
        # Récupérer la liste des tables dans la base de données
        table_names = self.db_manager.get_table_names()

        # Ajouter une racine à l'arbre
        root_item = QTreeWidgetItem(self.tree_db)
        root_item.setText(0, "Racine")

        wag_item = QTreeWidgetItem(root_item)
        wag_item.setText(0, "Wagtail")

        user_item = QTreeWidgetItem(root_item)
        user_item.setText(0, "User")
        prefix_tree = ["wagtailcore", "django", "auth", "taggit", "sqlite"]
        # Ajouter chaque table comme enfant de la racine
        for table in table_names:
            prefix = table.split('_')[0]
            if prefix in prefix_tree or prefix.startswith("wagtail"):
                child_item = QTreeWidgetItem(wag_item)
                child_item.setText(0, table)
            else:
                child_item = QTreeWidgetItem(user_item)
                child_item.setText(0, table)

    def closeEvent(self, event):
        # Fermer la connexion lorsque la fenêtre se ferme
        if self.db_manager is not None:
            self.db_manager.close_connection()
        if self.progress_dialog is not None:
            self.progress_dialog.deleteLater()

        self.stop_task()
        event.accept()

    def stop_task(self):

        if self.creator:
            self.creator.progress.disconnect(self.update_progress)
            self.creator = None

        if self.command:
            self.command.cancel_task()
            # self.command.status.disconnect(self.update_command)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec())
