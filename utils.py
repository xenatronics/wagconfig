import sys
import subprocess
import time
import os
from enum import Enum
from PySide6.QtCore import Qt, QRunnable, QThreadPool, QObject, Signal, Slot


class Couleur(Enum):
    ERROR = 2
    SUCCESS = 1
    NEUTRAL = 0


class Command(Enum):
    MIGRATIONS = 0
    MIGRATE = 1
    RUNSERVER = 6


class CommandCreator(QRunnable, QObject):
    file_requirements: str
    status = Signal(int, str, int)
    cancel_signal = Signal()
    browser_start = Signal()

    def __init__(self, path_dir, command=None):
        QObject.__init__(self)
        QRunnable.__init__(self)
        self.path_dir = path_dir
        self.command = command
        self.value_progress = 0
        self.is_cancelled = False
        # self.django_process = None

    # def cancel_task(self):
    #     self.is_cancelled = True

    def run(self):
        try:
            cmd = ""
            if self.command:
                if self.command == Command.RUNSERVER:
                    cmd = "python manage.py runserver"
                if self.command == Command.MIGRATIONS:
                    cmd = "python manage.py makemigrations"
                if self.command == Command.MIGRATE:
                    cmd = "python manage.py migrate"

                cmd = cmd.split()
                if cmd:
                    django_process = subprocess.Popen(
                        cmd,
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                        cwd=self.path_dir
                    )

                    while django_process.poll() is None:
                        # if self.is_cancelled:
                        #     django_process.terminate()  # Terminer le processus si annulé
                        #     break

                        line = django_process.stdout.readline().decode("utf-8").strip()

                        if line:
                            self.status.emit(self.value_progress, f"{line}<br>", 0)
                            if line.startswith("Quit"):
                                self.browser_start.emit()

        except subprocess.CalledProcessError as e:
            # Gestion des erreurs pour la commande Django
            self.status.emit(0, f"Erreur lors de l'exécution de la commande Django : {e}<br>", 0)
        finally:
            if self.is_cancelled:
                self.status.emit(self.value_progress, "Tâche annulée<br>", 0)
                self.is_cancelled = False
            else:
                self.status.emit(self.value_progress, "Tâche terminée<br>", 0)


class ApplicationCreator(QRunnable, QObject):
    file_requirements: str
    progress = Signal(int, str, int)

    def __init__(self, appname, path_dir, env=False, *args, **kwargs):
        QObject.__init__(self)
        QRunnable.__init__(self)
        self.env = env
        self.args = args
        self.kwargs = kwargs
        self.value_progress = 0
        self.appname = appname
        self.path_dir = path_dir
        self.file_requirements = "requirements.txt"

    @Slot()
    def run(self):

        try:
            process = subprocess.Popen(["wagtail", "start", self.appname, self.path_dir],
                                       stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            while process.poll() is None:
                line = process.stdout.readline().decode("utf-8").strip()
                if line:
                    self.value_progress = 20
                    # Émettre le signal pour mettre à jour la fenêtre de progression
                    self.progress.emit(self.value_progress,
                                       f"Création de l'application {self.appname} avec paramètres par défaut<br>", 0)

            # Émettre un signal pour indiquer la fin du processus
            process.wait()
            self.value_progress += 30
            self.progress.emit(self.value_progress, "L'application a été créée avec succès<br>", 0)

            if not self.env:
                # python_executable = sys.executable
                self.value_progress += 10
                self.progress.emit(self.value_progress, "Chargement ou mise à jour des modules", 0)
                os.chdir(self.path_dir)
                result = subprocess.run(["pip", "install", "-r", "requirements.txt"], capture_output=True, text=True)
                self.value_progress += 20

                message = result.stdout.replace("Requirement already satisfied:", "Module déjà installé:")
                message = message.replace("\n", "<br>")
                self.progress.emit(self.value_progress, message, 2)

                self.value_progress += 10
                self.progress.emit(self.value_progress, "Fin de chargement ou mise à jour des modules", 0)

                if result.returncode == 0:
                    print("Terminé avec succès:", result.returncode == 0)
                # Afficher la sortie d'erreur en cas d'échec
                else:
                    print("Erreur:")
                    print(result.stderr)
            else:
                pass
            time.sleep(1)
            self.progress.emit(100, f"L'application est disponible dans le dossier {self.path_dir} <br>", 0)

        except subprocess.CalledProcessError as e:
            self.progress.emit(0, f"Erreur lors de la création de l'application : {e}\n", 0)
