import sys
import subprocess
import time
import os


class WagTail:

    def __init__(self, path_dir, venv_name="venv", file_requirements="requirements.txt"):
        self.path_dir = path_dir
        self.file_requirements = file_requirements
        self.venv_name = venv_name
        self.appname = ""

    def create(self, appname):
        self.appname = appname
        try:
            # result = subprocess.check_output(["wagtail", "start", self.appname, self.path_dir]).decode("utf-8")
            process = subprocess.Popen(["wagtail", "start", self.appname, self.path_dir], stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
            process.wait()
            return "L'application est installé avec succès"
        except subprocess.CalledProcessError:
            return "L'application est déjà installé."

    def setup(self):
        try:
            os.chdir(self.path_dir)
            result = subprocess.check_output(["pip", "install", "-r", self.file_requirements]).decode("utf-8")
            return result
        except subprocess.CalledProcessError:
            return "Les modules ne sont pas installés."

    def create_env(self):
        try:
            # Créer le répertoire pour l'environnement virtuel
            venv_path = os.path.join(self.path_dir, self.venv_name)
            os.makedirs(venv_path, exist_ok=True)
            # Créer l'environnement virtuel
            os.chdir(self.path_dir)
            process = subprocess.Popen(["python", "-m", "venv", self.venv_name], stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
            process.wait()
            # subprocess.run(["python", "-m", "venv", self.venv_name], check=True)

            return f"Environnement virtuel créé avec succès à {venv_path}"
        except subprocess.CalledProcessError as e:
            return f"Création impossible de l'environnement virtuel"
        except Exception as e:
            return (f"Une erreur inattendue s'est produite : {e}")

    def activate_env(self):
        try:
            # Créer le répertoire pour l'environnement virtuel
            venv_path = os.path.join(self.path_dir, self.venv_name)
            # Activer l'environnement virtuel
            os.chdir(venv_path + "\\Scripts")
            result = subprocess.run(["activate.bat"], check=True)
            return f"Environnement virtuel activé avec succès à {venv_path}"

        except subprocess.CalledProcessError as e:
            return f"Erreur lors de l'activation de l'environnement virtuel : {e}"

        except Exception as e:
            return (f"Une erreur inattendue s'est produite : {e}")

    def run_cmd_env(self, command):
        try:
            # Chemin complet de l'environnement virtuel
            venv_path = os.path.abspath(os.path.join(self.path_dir, self.venv_name))
            # Construire le chemin complet vers l'exécutable pip dans l'environnement virtuel
            pip_path = os.path.join(venv_path, "Scripts" if os.name == "nt" else "bin", "pip")
            # Exécuter la commande pip dans l'environnement virtuel
            command = command.split()
            os.chdir(self.path_dir)
            output = subprocess.check_output(command).decode("utf-8")
            return output
        except subprocess.CalledProcessError as e:
            return f"Exécution impossible de la commande pip : {e}"
        except Exception as e:
            return f"Une erreur inattendue s'est produite : {e}"

    def run_requirements(self):
        try:
            os.chdir(self.path_dir)

            # Utiliser le chemin complet vers l'exécutable pip dans l'environnement virtuel
            pip_path = os.path.join(self.path_dir, self.venv_name, "Scripts" if os.name == "nt" else "bin", "pip")
            result = subprocess.check_output([pip_path, "install", "-r", self.file_requirements]).decode("utf-8")
            return result
        except subprocess.CalledProcessError:
            return "Les modules ne sont pas installés."
        except Exception as e:
            return f"Une erreur inattendue s'est produite : {e}"

    def upgrade_pip(self):
        try:
            os.chdir(self.path_dir)
            # Utiliser le chemin complet vers l'exécutable pip dans l'environnement virtuel
            pip_path = os.path.join(self.path_dir, self.venv_name, "Scripts" if os.name == "nt" else "bin", "pip")
            # Exécuter la commande de mise à jour de pip avec stdout redirigé
            process = subprocess.Popen([pip_path, "install", "--upgrade", "pip"], stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
            # Attendre la fin de l'exécution
            process.wait()
            # Afficher les messages de sortie
            stdout = process.stdout.read().decode("utf-8")
            stderr = process.stderr.read().decode("utf-8")
            return stdout, stderr

        except subprocess.CalledProcessError as e:
            return f"Erreur lors de la mise à jour de pip : {e}"

        except Exception as e:
            return f"Une erreur inattendue s'est produite : {e}"


if __name__ == "__main__":
    path = "E:\\projets wagtail\\test"
    # wagtail = WagTail(path, "myenv")
    # msg = wagtail.create_env()
    # print(msg)
    # msg = wagtail.activate_env()
    # print(msg)
    # msg = wagtail.create("wagtailtest")
    # print(msg)
    # msg, _ = wagtail.upgrade2()
    # print(msg)
    # msg = wagtail.run_requirements()
    # print(msg)
