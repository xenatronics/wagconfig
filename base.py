# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'wagtailconfiglNjPWU.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWebEngineWidgets import QWebEngineView
from PySide6.QtWidgets import (QApplication, QCheckBox, QFrame, QGridLayout,
    QHBoxLayout, QHeaderView, QLabel, QLayout,
    QListWidget, QListWidgetItem, QMainWindow, QMenuBar,
    QPushButton, QScrollArea, QSizePolicy, QSpacerItem,
    QSpinBox, QSplitter, QStackedWidget, QStatusBar,
    QTabWidget, QTableView, QTextEdit, QTreeWidget,
    QTreeWidgetItem, QVBoxLayout, QWidget)

class Ui_WagConfig(object):
    def setupUi(self, WagConfig):
        if not WagConfig.objectName():
            WagConfig.setObjectName(u"WagConfig")
        WagConfig.resize(1166, 800)
        icon = QIcon()
        icon.addFile(u"C:/Users/p-gar/Downloads/wizard.png", QSize(), QIcon.Normal, QIcon.Off)
        WagConfig.setWindowIcon(icon)
        WagConfig.setUnifiedTitleAndToolBarOnMac(False)
        self.centralwidget = QWidget(WagConfig)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout_9 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setSizeConstraint(QLayout.SetMaximumSize)
        self.verticalLayout.setContentsMargins(-1, -1, -1, 9)
        self.splitter = QSplitter(self.centralwidget)
        self.splitter.setObjectName(u"splitter")
        self.splitter.setEnabled(True)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(3)
        sizePolicy.setHeightForWidth(self.splitter.sizePolicy().hasHeightForWidth())
        self.splitter.setSizePolicy(sizePolicy)
        self.splitter.setBaseSize(QSize(0, 0))
        self.splitter.setFrameShape(QFrame.StyledPanel)
        self.splitter.setMidLineWidth(2)
        self.splitter.setOrientation(Qt.Horizontal)
        self.splitter.setHandleWidth(2)
        self.splitter.setChildrenCollapsible(True)
        self.widget = QWidget(self.splitter)
        self.widget.setObjectName(u"widget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(1)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy1)
        self.widget.setMaximumSize(QSize(500, 16777215))
        self.widget.setStyleSheet(u"background:rgb(255, 255, 255)")
        self.verticalLayout_3 = QVBoxLayout(self.widget)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setSizeConstraint(QLayout.SetMinAndMaxSize)
        self.tabWidget = QTabWidget(self.widget)
        self.tabWidget.setObjectName(u"tabWidget")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(1)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy2)
        self.tabWidget.setMinimumSize(QSize(250, 0))
        self.tabWidget.setMaximumSize(QSize(16777215, 16777211))
        self.tabWidget.setAutoFillBackground(False)
        self.tabWidget.setStyleSheet(u"/* Styles pour les onglets */\n"
"QTabBar::tab {\n"
"    background-color: rgb(59, 59, 59);\n"
"    color: rgb(255, 255, 255);\n"
"    border: 1px solid rgb(59, 59, 59);\n"
"    border-radius: 6px;\n"
"    padding: 5px 10px;\n"
"}\n"
"\n"
"/* Styles pour le contenu des onglets (widgets associ\u00e9s) */\n"
"QTabWidget::pane {\n"
"    background-color: black;\n"
"	border:1px solid lightgray;\n"
"}\n"
"\n"
"/* Styles pour les onglets */\n"
"QTabBar::tab:selected {\n"
"    background-color: rgb(59, 59, 59); /* Couleur de s\u00e9lection plus claire */\n"
"    color: white;\n"
"    border: 1px solid rgb(85, 255, 0);\n"
"    border-radius: 6px;\n"
"    padding: 5px 10px;\n"
"}\n"
"\n"
"")
        self.tab_application = QWidget()
        self.tab_application.setObjectName(u"tab_application")
        self.tab_application.setMinimumSize(QSize(223, 0))
        self.tab_application.setMaximumSize(QSize(229, 459))
        self.verticalLayoutWidget = QWidget(self.tab_application)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(10, 20, 251, 172))
        self.verticalLayout_8 = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(u"label")

        self.verticalLayout_8.addWidget(self.label)

        self.python_version = QLabel(self.verticalLayoutWidget)
        self.python_version.setObjectName(u"python_version")

        self.verticalLayout_8.addWidget(self.python_version)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_8.addItem(self.verticalSpacer_3)

        self.label_2 = QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout_8.addWidget(self.label_2)

        self.python_path = QLabel(self.verticalLayoutWidget)
        self.python_path.setObjectName(u"python_path")

        self.verticalLayout_8.addWidget(self.python_path)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_8.addItem(self.verticalSpacer_4)

        self.label_6 = QLabel(self.verticalLayoutWidget)
        self.label_6.setObjectName(u"label_6")

        self.verticalLayout_8.addWidget(self.label_6)

        self.wagtail_version = QLabel(self.verticalLayoutWidget)
        self.wagtail_version.setObjectName(u"wagtail_version")

        self.verticalLayout_8.addWidget(self.wagtail_version)

        self.tabWidget.addTab(self.tab_application, "")
        self.tab_tools = QWidget()
        self.tab_tools.setObjectName(u"tab_tools")
        self.verticalLayoutWidget_2 = QWidget(self.tab_tools)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(10, 20, 251, 291))
        self.verticalLayout_10 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.verticalLayout_10.setContentsMargins(0, 0, 0, 0)
        self.pushButton_5 = QPushButton(self.verticalLayoutWidget_2)
        self.pushButton_5.setObjectName(u"pushButton_5")
        self.pushButton_5.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(59, 59, 59);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"}")

        self.verticalLayout_10.addWidget(self.pushButton_5)

        self.checkBox_3 = QCheckBox(self.verticalLayoutWidget_2)
        self.checkBox_3.setObjectName(u"checkBox_3")
        self.checkBox_3.setStyleSheet(u"QCheckBox::indicator:checked {\n"
"    background-color:rgba(76, 227, 0,62);  /* Couleur de fond de la checkbox lorsqu'elle est coch\u00e9e */\n"
"    border: 1px solid rgb(76, 227, 0);  /* Bordure de la checkbox lorsqu'elle est\n"
" coch\u00e9e */\n"
"border-radius:3px;\n"
"}\n"
"")
        self.checkBox_3.setChecked(True)

        self.verticalLayout_10.addWidget(self.checkBox_3)

        self.checkBox_4 = QCheckBox(self.verticalLayoutWidget_2)
        self.checkBox_4.setObjectName(u"checkBox_4")
        self.checkBox_4.setStyleSheet(u"QCheckBox::indicator:checked {\n"
"    background-color:rgba(76, 227, 0,62);  /* Couleur de fond de la checkbox lorsqu'elle est coch\u00e9e */\n"
"    border: 1px solid rgb(76, 227, 0);  /* Bordure de la checkbox lorsqu'elle est\n"
" coch\u00e9e */\n"
"border-radius:3px;\n"
"}\n"
"")

        self.verticalLayout_10.addWidget(self.checkBox_4)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_10.addItem(self.verticalSpacer_2)

        self.pushButton_6 = QPushButton(self.verticalLayoutWidget_2)
        self.pushButton_6.setObjectName(u"pushButton_6")
        self.pushButton_6.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(59, 59, 59);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"}")

        self.verticalLayout_10.addWidget(self.pushButton_6)

        self.listWidget = QListWidget(self.verticalLayoutWidget_2)
        self.listWidget.setObjectName(u"listWidget")
        self.listWidget.setStyleSheet(u"background-color: rgb(40, 40, 40);\n"
"border: 1px solid white;\n"
"border-radius:4px;")

        self.verticalLayout_10.addWidget(self.listWidget)

        icon1 = QIcon(QIcon.fromTheme(u"folder-new"))
        self.tabWidget.addTab(self.tab_tools, icon1, "")
        self.tab_database = QWidget()
        self.tab_database.setObjectName(u"tab_database")
        self.verticalLayoutWidget_4 = QWidget(self.tab_database)
        self.verticalLayoutWidget_4.setObjectName(u"verticalLayoutWidget_4")
        self.verticalLayoutWidget_4.setGeometry(QRect(0, 20, 258, 401))
        self.verticalLayout_7 = QVBoxLayout(self.verticalLayoutWidget_4)
        self.verticalLayout_7.setSpacing(6)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setSizeConstraint(QLayout.SetMaximumSize)
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.pushButton_4 = QPushButton(self.verticalLayoutWidget_4)
        self.pushButton_4.setObjectName(u"pushButton_4")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(1)
        sizePolicy3.setHeightForWidth(self.pushButton_4.sizePolicy().hasHeightForWidth())
        self.pushButton_4.setSizePolicy(sizePolicy3)
        self.pushButton_4.setMinimumSize(QSize(0, 34))
        self.pushButton_4.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(59, 59, 59);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"}")

        self.verticalLayout_7.addWidget(self.pushButton_4)

        self.textEdit = QTextEdit(self.verticalLayoutWidget_4)
        self.textEdit.setObjectName(u"textEdit")
        sizePolicy4 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.textEdit.sizePolicy().hasHeightForWidth())
        self.textEdit.setSizePolicy(sizePolicy4)
        self.textEdit.setMinimumSize(QSize(0, 30))
        self.textEdit.setMaximumSize(QSize(16777215, 30))
        self.textEdit.setBaseSize(QSize(0, 0))
        self.textEdit.setStyleSheet(u"border: 2px solid black;\n"
"border-radius: 4px;")

        self.verticalLayout_7.addWidget(self.textEdit)

        self.btn_connect_db = QPushButton(self.verticalLayoutWidget_4)
        self.btn_connect_db.setObjectName(u"btn_connect_db")
        sizePolicy3.setHeightForWidth(self.btn_connect_db.sizePolicy().hasHeightForWidth())
        self.btn_connect_db.setSizePolicy(sizePolicy3)
        self.btn_connect_db.setMinimumSize(QSize(0, 34))
        self.btn_connect_db.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(59, 59, 59);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"}")

        self.verticalLayout_7.addWidget(self.btn_connect_db)

        self.verticalSpacer = QSpacerItem(20, 0, QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.verticalLayout_7.addItem(self.verticalSpacer)

        self.tree_db = QTreeWidget(self.verticalLayoutWidget_4)
        self.tree_db.setObjectName(u"tree_db")
        sizePolicy2.setHeightForWidth(self.tree_db.sizePolicy().hasHeightForWidth())
        self.tree_db.setSizePolicy(sizePolicy2)
        self.tree_db.setStyleSheet(u"QTreeWidget{\n"
"background-color: rgb(60, 60, 60);\n"
"color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QTreeWidget::item:selected {\n"
"    background: transparent; /* Fond transparent */\n"
"	 color: rgb(85, 255, 127); /* Couleur du texte au survol */\n"
"}\n"
"\n"
"\n"
"QTreeWidget::item:hover {\n"
"    background: rgba(85, 255, 0, 5); /* Fond transparent */\n"
"    color: white; /* Couleur du texte au survol */\n"
"}\n"
"\n"
"QTreeWidget::item:selected:hover {\n"
"    background: transparent; /* Fond transparent */\n"
"    color: rgb(85, 255, 127); /* Couleur du texte au survol */\n"
"}")
        self.tree_db.setTabKeyNavigation(True)
        self.tree_db.header().setVisible(False)

        self.verticalLayout_7.addWidget(self.tree_db)

        self.verticalLayout_7.setStretch(0, 1)
        self.tabWidget.addTab(self.tab_database, "")
        self.tab_server = QWidget()
        self.tab_server.setObjectName(u"tab_server")
        self.pushButton_8 = QPushButton(self.tab_server)
        self.pushButton_8.setObjectName(u"pushButton_8")
        self.pushButton_8.setGeometry(QRect(20, 10, 251, 28))
        self.pushButton_8.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(59, 59, 59);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"}\n"
"")
        self.btn_start_server = QPushButton(self.tab_server)
        self.btn_start_server.setObjectName(u"btn_start_server")
        self.btn_start_server.setGeometry(QRect(20, 100, 249, 41))
        self.btn_start_server.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(40, 40, 40);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:8px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"cursor:pointer;\n"
"}")
        self.txt_address_server = QTextEdit(self.tab_server)
        self.txt_address_server.setObjectName(u"txt_address_server")
        self.txt_address_server.setGeometry(QRect(20, 50, 171, 31))
        self.txt_address_server.setStyleSheet(u"border: 2px solid rgb(40, 40, 40);\n"
"border-radius: 4px;\n"
"color:rgb(40, 40, 40);\n"
"background-color:white;")
        self.spin_port_server = QSpinBox(self.tab_server)
        self.spin_port_server.setObjectName(u"spin_port_server")
        self.spin_port_server.setGeometry(QRect(210, 50, 61, 31))
        self.spin_port_server.setStyleSheet(u"border: 2px solid rgb(40,40,40);\n"
"border-radius: 4px;\n"
"color:rgb(40,40,40);")
        self.spin_port_server.setMinimum(8000)
        self.spin_port_server.setMaximum(8010)
        self.btn_start_admin = QPushButton(self.tab_server)
        self.btn_start_admin.setObjectName(u"btn_start_admin")
        self.btn_start_admin.setGeometry(QRect(20, 150, 249, 41))
        self.btn_start_admin.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(40, 40, 40);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:8px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"cursor:pointer;\n"
"}")
        self.tabWidget.addTab(self.tab_server, "")

        self.verticalLayout_3.addWidget(self.tabWidget)

        self.splitter.addWidget(self.widget)
        self.widget_2 = QWidget(self.splitter)
        self.widget_2.setObjectName(u"widget_2")
        sizePolicy5 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy5.setHorizontalStretch(5)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.widget_2.sizePolicy().hasHeightForWidth())
        self.widget_2.setSizePolicy(sizePolicy5)
        self.widget_2.setStyleSheet(u"background:rgb(255, 255, 255)")
        self.verticalLayout_2 = QVBoxLayout(self.widget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.pushButton_7 = QPushButton(self.widget_2)
        self.pushButton_7.setObjectName(u"pushButton_7")
        sizePolicy6 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.pushButton_7.sizePolicy().hasHeightForWidth())
        self.pushButton_7.setSizePolicy(sizePolicy6)
        self.pushButton_7.setLayoutDirection(Qt.RightToLeft)
        self.pushButton_7.setAutoFillBackground(False)
        self.pushButton_7.setStyleSheet(u"background-color: rgb(40, 40, 40);\n"
"color: rgb(255, 255, 255);\n"
"border-color: rgb(85, 255, 0);\n"
"border-radius:6px;\n"
"padding:5px 10px;")
        self.pushButton_7.setIcon(icon)
        self.pushButton_7.setFlat(False)

        self.verticalLayout_2.addWidget(self.pushButton_7)

        self.widget_3 = QWidget(self.widget_2)
        self.widget_3.setObjectName(u"widget_3")
        sizePolicy7 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.widget_3.sizePolicy().hasHeightForWidth())
        self.widget_3.setSizePolicy(sizePolicy7)
        self.verticalLayout_5 = QVBoxLayout(self.widget_3)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setSizeConstraint(QLayout.SetMinAndMaxSize)
        self.stackedWidget = QStackedWidget(self.widget_3)
        self.stackedWidget.setObjectName(u"stackedWidget")
        sizePolicy8 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy8.setHorizontalStretch(0)
        sizePolicy8.setVerticalStretch(1)
        sizePolicy8.setHeightForWidth(self.stackedWidget.sizePolicy().hasHeightForWidth())
        self.stackedWidget.setSizePolicy(sizePolicy8)
        self.stackedWidget.setStyleSheet(u"background-color: rgb(60, 60, 60);\n"
"color:white;")
        self.page = QWidget()
        self.page.setObjectName(u"page")
        self.text_name_app = QTextEdit(self.page)
        self.text_name_app.setObjectName(u"text_name_app")
        self.text_name_app.setGeometry(QRect(160, 31, 316, 31))
        self.text_name_app.setStyleSheet(u"border: 2px solid white;\n"
"border-radius: 4px;\n"
"color:white;\n"
"background-color:rgb(40, 40, 40);")
        self.label_3 = QLabel(self.page)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(20, 40, 117, 16))
        self.browse_path_app = QPushButton(self.page)
        self.browse_path_app.setObjectName(u"browse_path_app")
        self.browse_path_app.setGeometry(QRect(490, 70, 30, 30))
        self.browse_path_app.setStyleSheet(u"background-color: rgb(40, 40, 40);\n"
"color: rgb(255, 255, 255);\n"
"border: 2px solid white;\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"color:white;")
        self.text_path_app = QTextEdit(self.page)
        self.text_path_app.setObjectName(u"text_path_app")
        self.text_path_app.setGeometry(QRect(160, 70, 316, 31))
        self.text_path_app.setStyleSheet(u"border: 2px solid white;\n"
"border-radius: 4px;\n"
"color:white;\n"
"background-color:rgb(40, 40, 40);")
        self.label_4 = QLabel(self.page)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(20, 140, 117, 31))
        self.btn_create_app = QPushButton(self.page)
        self.btn_create_app.setObjectName(u"btn_create_app")
        self.btn_create_app.setGeometry(QRect(160, 230, 81, 26))
        self.btn_create_app.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(40, 40, 40);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"}")
        self.label_5 = QLabel(self.page)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(20, 79, 131, 16))
        self.chk_venv = QCheckBox(self.page)
        self.chk_venv.setObjectName(u"chk_venv")
        self.chk_venv.setGeometry(QRect(160, 110, 142, 16))
        self.chk_venv.setStyleSheet(u"QCheckBox::indicator:checked {\n"
"    background-color:rgba(76, 227, 0,62);  /* Couleur de fond de la checkbox lorsqu'elle est coch\u00e9e */\n"
"    border: 1px solid rgb(76, 227, 0);  /* Bordure de la checkbox lorsqu'elle est\n"
" coch\u00e9e */\n"
"border-radius:3px;\n"
"}\n"
"")
        self.layoutWidget = QWidget(self.page)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(160, 150, 291, 40))
        self.gridLayout = QGridLayout(self.layoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.chk_menus = QCheckBox(self.layoutWidget)
        self.chk_menus.setObjectName(u"chk_menus")
        self.chk_menus.setStyleSheet(u"QCheckBox::indicator:checked {\n"
"    background-color:rgba(76, 227, 0,62);  /* Couleur de fond de la checkbox lorsqu'elle est coch\u00e9e */\n"
"    border: 1px solid rgb(76, 227, 0);  /* Bordure de la checkbox lorsqu'elle est\n"
" coch\u00e9e */\n"
"border-radius:3px;\n"
"}\n"
"")

        self.gridLayout.addWidget(self.chk_menus, 0, 0, 1, 1)

        self.chk_login = QCheckBox(self.layoutWidget)
        self.chk_login.setObjectName(u"chk_login")
        self.chk_login.setStyleSheet(u"QCheckBox::indicator:checked {\n"
"    background-color:rgba(76, 227, 0,62);  /* Couleur de fond de la checkbox lorsqu'elle est coch\u00e9e */\n"
"    border: 1px solid rgb(76, 227, 0);  /* Bordure de la checkbox lorsqu'elle est\n"
" coch\u00e9e */\n"
"border-radius:3px;\n"
"}\n"
"")

        self.gridLayout.addWidget(self.chk_login, 0, 1, 1, 1)

        self.chck_auth = QCheckBox(self.layoutWidget)
        self.chck_auth.setObjectName(u"chck_auth")
        self.chck_auth.setStyleSheet(u"QCheckBox::indicator:checked {\n"
"    background-color:rgba(76, 227, 0,62);  /* Couleur de fond de la checkbox lorsqu'elle est coch\u00e9e */\n"
"    border: 1px solid rgb(76, 227, 0);  /* Bordure de la checkbox lorsqu'elle est\n"
" coch\u00e9e */\n"
"border-radius:3px;\n"
"}\n"
"")

        self.gridLayout.addWidget(self.chck_auth, 1, 0, 1, 1)

        self.chk_social = QCheckBox(self.layoutWidget)
        self.chk_social.setObjectName(u"chk_social")
        self.chk_social.setStyleSheet(u"QCheckBox::indicator:checked {\n"
"    background-color:rgba(76, 227, 0,62);  /* Couleur de fond de la checkbox lorsqu'elle est coch\u00e9e */\n"
"    border: 1px solid rgb(76, 227, 0);  /* Bordure de la checkbox lorsqu'elle est\n"
" coch\u00e9e */\n"
"border-radius:3px;\n"
"}\n"
"")

        self.gridLayout.addWidget(self.chk_social, 1, 1, 1, 1)

        self.stackedWidget.addWidget(self.page)
        self.page_2 = QWidget()
        self.page_2.setObjectName(u"page_2")
        self.pushButton_10 = QPushButton(self.page_2)
        self.pushButton_10.setObjectName(u"pushButton_10")
        self.pushButton_10.setGeometry(QRect(60, 50, 141, 26))
        self.pushButton_10.setStyleSheet(u"background-color: rgb(40, 40, 40);\n"
"color: rgb(255, 255, 255);\n"
"border:1px solid white;\n"
"border-radius:6px;\n"
"padding:5px 10px;")
        self.pushButton_11 = QPushButton(self.page_2)
        self.pushButton_11.setObjectName(u"pushButton_11")
        self.pushButton_11.setGeometry(QRect(60, 90, 141, 26))
        self.pushButton_11.setStyleSheet(u"background-color: rgb(40, 40, 40);\n"
"color: rgb(255, 255, 255);\n"
"border:1px solid white;\n"
"border-radius:6px;\n"
"padding:5px 10px;")
        self.stackedWidget.addWidget(self.page_2)
        self.page_3 = QWidget()
        self.page_3.setObjectName(u"page_3")
        self.page_3.setMaximumSize(QSize(757, 433))
        self.verticalLayout_12 = QVBoxLayout(self.page_3)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.table_db = QTableView(self.page_3)
        self.table_db.setObjectName(u"table_db")
        self.table_db.setStyleSheet(u" QTableWidget {\n"
"                background-color: #2E2E2E;\n"
"                color: #FFFFFF;\n"
"                selection-background-color: #007F00;  /* Vert pour la s\u00e9lection */\n"
"            }\n"
"            QTableWidget::item:selected {\n"
"                background-color: #3A3A3A;\n"
"            }\n"
"            QHeaderView::section {\n"
"                background-color: #454545;\n"
"                color: #FFFFFF;\n"
"                border: 1px solid #555555;\n"
"            }")
        self.table_db.horizontalHeader().setDefaultSectionSize(150)
        self.table_db.verticalHeader().setStretchLastSection(False)

        self.verticalLayout_12.addWidget(self.table_db)

        self.stackedWidget.addWidget(self.page_3)
        self.page_4 = QWidget()
        self.page_4.setObjectName(u"page_4")
        sizePolicy7.setHeightForWidth(self.page_4.sizePolicy().hasHeightForWidth())
        self.page_4.setSizePolicy(sizePolicy7)
        self.verticalLayout_11 = QVBoxLayout(self.page_4)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.view_server = QWebEngineView(self.page_4)
        self.view_server.setObjectName(u"view_server")
        sizePolicy7.setHeightForWidth(self.view_server.sizePolicy().hasHeightForWidth())
        self.view_server.setSizePolicy(sizePolicy7)
        self.view_server.setUrl(QUrl(u"about:blank"))
        self.view_server.setZoomFactor(0.800000000000000)

        self.verticalLayout_11.addWidget(self.view_server)

        self.stackedWidget.addWidget(self.page_4)

        self.verticalLayout_5.addWidget(self.stackedWidget)


        self.verticalLayout_2.addWidget(self.widget_3)

        self.splitter.addWidget(self.widget_2)

        self.verticalLayout.addWidget(self.splitter)

        self.verticalLayout_6 = QVBoxLayout()
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")

        self.verticalLayout.addLayout(self.verticalLayout_6)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.pushButton_2 = QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(59, 59, 59);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"}")

        self.horizontalLayout_3.addWidget(self.pushButton_2)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.pushButton_3 = QPushButton(self.centralwidget)
        self.pushButton_3.setObjectName(u"pushButton_3")
        self.pushButton_3.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(59, 59, 59);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"}")

        self.horizontalLayout_3.addWidget(self.pushButton_3)

        self.pushButton = QPushButton(self.centralwidget)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setStyleSheet(u"QPushButton{\n"
"background-color: rgb(59, 59, 59);\n"
"color: rgb(255, 255, 255);\n"
"border: 1px solid rgb(255, 255, 250);\n"
"border-radius:6px;\n"
"padding:5px 10px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(85, 255, 0);\n"
"color:rgb(85, 255, 0);\n"
"}")

        self.horizontalLayout_3.addWidget(self.pushButton)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.scrollArea = QScrollArea(self.centralwidget)
        self.scrollArea.setObjectName(u"scrollArea")
        sizePolicy8.setHeightForWidth(self.scrollArea.sizePolicy().hasHeightForWidth())
        self.scrollArea.setSizePolicy(sizePolicy8)
        self.scrollArea.setMinimumSize(QSize(0, 0))
        self.scrollArea.setStyleSheet(u"")
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 1127, 166))
        self.scrollAreaWidgetContents.setStyleSheet(u"background-color: rgb(0, 0, 0);")
        self.verticalLayout_13 = QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.lbl_output = QLabel(self.scrollAreaWidgetContents)
        self.lbl_output.setObjectName(u"lbl_output")
        self.lbl_output.setAutoFillBackground(False)
        self.lbl_output.setStyleSheet(u"background-color: rgb(0, 0, 0);\n"
"color:white;\n"
"padding:8px;")
        self.lbl_output.setFrameShape(QFrame.NoFrame)
        self.lbl_output.setScaledContents(False)
        self.lbl_output.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.lbl_output.setWordWrap(False)
        self.lbl_output.setMargin(4)
        self.lbl_output.setOpenExternalLinks(True)

        self.verticalLayout_13.addWidget(self.lbl_output)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.verticalLayout.addWidget(self.scrollArea)

        self.verticalLayout_4 = QVBoxLayout()
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")

        self.verticalLayout.addLayout(self.verticalLayout_4)


        self.verticalLayout_9.addLayout(self.verticalLayout)

        WagConfig.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(WagConfig)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1166, 22))
        WagConfig.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(WagConfig)
        self.statusbar.setObjectName(u"statusbar")
        WagConfig.setStatusBar(self.statusbar)

        self.retranslateUi(WagConfig)
        self.tabWidget.tabBarClicked.connect(self.stackedWidget.setCurrentIndex)

        self.tabWidget.setCurrentIndex(3)
        self.stackedWidget.setCurrentIndex(3)


        QMetaObject.connectSlotsByName(WagConfig)
    # setupUi

    def retranslateUi(self, WagConfig):
        WagConfig.setWindowTitle(QCoreApplication.translate("WagConfig", u"WagConfig", None))
        self.label.setText(QCoreApplication.translate("WagConfig", u"<html><head/><body><p><span style=\" font-weight:700;\">Version Python :</span></p></body></html>", None))
        self.python_version.setText(QCoreApplication.translate("WagConfig", u"Version Python:", None))
        self.label_2.setText(QCoreApplication.translate("WagConfig", u"<html><head/><body><p><span style=\" font-weight:700;\">Chemin Python:</span></p></body></html>", None))
        self.python_path.setText(QCoreApplication.translate("WagConfig", u"Version Python:", None))
        self.label_6.setText(QCoreApplication.translate("WagConfig", u"<html><head/><body><p><span style=\" font-weight:700;\">Version Wagtail:</span></p></body></html>", None))
        self.wagtail_version.setText(QCoreApplication.translate("WagConfig", u"Version Wagtail :", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_application), QCoreApplication.translate("WagConfig", u"Cr\u00e9ation", None))
        self.pushButton_5.setText(QCoreApplication.translate("WagConfig", u"Nouvelle application", None))
        self.checkBox_3.setText(QCoreApplication.translate("WagConfig", u"templates", None))
        self.checkBox_4.setText(QCoreApplication.translate("WagConfig", u"templatetags", None))
        self.pushButton_6.setText(QCoreApplication.translate("WagConfig", u"Supprimer application", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_tools), QCoreApplication.translate("WagConfig", u"Outils", None))
        self.pushButton_4.setText(QCoreApplication.translate("WagConfig", u"..", None))
        self.btn_connect_db.setText(QCoreApplication.translate("WagConfig", u"Se connecter", None))
        ___qtreewidgetitem = self.tree_db.headerItem()
        ___qtreewidgetitem.setText(0, QCoreApplication.translate("WagConfig", u"Racine", None));
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_database), QCoreApplication.translate("WagConfig", u"Base de donn\u00e9es", None))
        self.pushButton_8.setText(QCoreApplication.translate("WagConfig", u"Param\u00e8tres serveur", None))
        self.btn_start_server.setText(QCoreApplication.translate("WagConfig", u"D\u00e9marrer serveur", None))
        self.txt_address_server.setHtml(QCoreApplication.translate("WagConfig", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">http://localhost</p></body></html>", None))
        self.btn_start_admin.setText(QCoreApplication.translate("WagConfig", u"Administrer", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_server), QCoreApplication.translate("WagConfig", u"Serveur", None))
        self.pushButton_7.setText(QCoreApplication.translate("WagConfig", u"Assistant     ", None))
        self.text_name_app.setHtml(QCoreApplication.translate("WagConfig", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">wagtest</p></body></html>", None))
        self.label_3.setText(QCoreApplication.translate("WagConfig", u"Nom de l'application :", None))
        self.browse_path_app.setText(QCoreApplication.translate("WagConfig", u"..", None))
        self.text_path_app.setHtml(QCoreApplication.translate("WagConfig", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">E:/projets wagtail/test</p></body></html>", None))
        self.label_4.setText(QCoreApplication.translate("WagConfig", u"Extensions :", None))
        self.btn_create_app.setText(QCoreApplication.translate("WagConfig", u"Cr\u00e9er", None))
        self.label_5.setText(QCoreApplication.translate("WagConfig", u"Chemin de l'application :", None))
        self.chk_venv.setText(QCoreApplication.translate("WagConfig", u"environnement virtuel", None))
        self.chk_menus.setText(QCoreApplication.translate("WagConfig", u"menu", None))
        self.chk_login.setText(QCoreApplication.translate("WagConfig", u"login", None))
        self.chck_auth.setText(QCoreApplication.translate("WagConfig", u"authentification", None))
        self.chk_social.setText(QCoreApplication.translate("WagConfig", u"social login", None))
        self.pushButton_10.setText(QCoreApplication.translate("WagConfig", u"Makemigrations", None))
        self.pushButton_11.setText(QCoreApplication.translate("WagConfig", u"Migrate", None))
        self.pushButton_2.setText(QCoreApplication.translate("WagConfig", u"Sortie", None))
        self.pushButton_3.setText(QCoreApplication.translate("WagConfig", u"Minimiser", None))
        self.pushButton.setText(QCoreApplication.translate("WagConfig", u"Effacer", None))
        self.lbl_output.setText("")
    # retranslateUi

