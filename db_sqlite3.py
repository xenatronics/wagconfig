import sqlite3
import os

class DatabaseManager:
    def __init__(self, database_path):
        self.database_path = database_path
        self.conn = None
        self.cursor = None

    def connect(self):
        self.conn = sqlite3.connect(self.database_path)
        self.cursor = self.conn.cursor()
        if self.cursor is not None:
            return True

        return False

    def close_connection(self):
        if self.conn:
            self.conn.close()

    def get_database_name(self):
        return os.path.basename(self.database_path)

    def get_table_names(self):
        if not self.conn:
            self.connect()

        self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        tables = self.cursor.fetchall()
        return [table[0] for table in tables]

    def get_table_data(self, table_name):
        if not self.conn:
            self.connect()

        # Récupérer les champs
        self.cursor.execute(f"PRAGMA table_info({table_name});")
        fields = [field[1] for field in self.cursor.fetchall()]

        # Récupérer les enregistrements
        self.cursor.execute(f"SELECT * FROM {table_name};")
        records = self.cursor.fetchall()

        return fields, records


if __name__ == "__main__":
    db= DatabaseManager("db.sqlite3")
    print(db.get_table_names())