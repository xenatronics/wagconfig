import sys
import subprocess


def show_python_version():
    python_version = sys.version.split()[0]
    return f'{python_version}'


def show_python_path():
    python_path = sys.executable
    return f"{python_path}"


def show_wagtail_version():
    try:
        # Exécutez la commande shell pour obtenir la version de Wagtail
        result = subprocess.check_output(["wagtail", "--version"]).decode("utf-8")
        wagtail_version = result.split("Wagtail")[1].strip()

        return f"{wagtail_version}"
    except subprocess.CalledProcessError:
        return "Wagtail n'est pas installé."


def is_wagtail_env():
    try:
        # Exécutez la commande shell pour obtenir la version de Wagtail
        result = subprocess.check_output(["pip", "show wagtail"]).decode("utf-8")
        result = result.splitlines()
        wagtail_version = result.split("Version")[1].strip()

        return f"Version (env) de Wagtail : {wagtail_version}"
    except subprocess.CalledProcessError:
        return "Wagtail (env) n'est pas installé."


if __name__ == "__main__":
    print(show_python_version())
    print(show_python_path())
    print(show_wagtail_version())
    print(is_wagtail_env())
